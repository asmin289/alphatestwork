import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

public class SortNumbers {
    static Path path = Paths.get("src/main/resources/data").toAbsolutePath();
    static int[] numbers;
    static {
        try {
            numbers = Files.lines(path)
                    .flatMap(e -> Stream.of(e.split(",")))
                    .mapToInt(Integer::parseInt)
                    .toArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void getSort() {
        Arrays.stream(numbers).sorted().forEach(s -> System.out.print(s + " "));
    }

    public static void getSortReverse() {
        Arrays.stream(numbers).boxed().sorted(Collections.reverseOrder()).forEach(s -> System.out.print(s + " "));
    }

    public static void main(String[] args) {
        System.out.println("Сортировка по возрастанию:");
        getSort();
        System.out.println("\n"+"Сортировка по убыванию:");
        getSortReverse();



    }

}
